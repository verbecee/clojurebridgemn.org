Welcome to ClojureBridge
========================

## Agenda
* [Friday](#agenda-for-friday)
* [Saturday](#agenda-for-saturday)

## Introductions

We'll go around the room and ask everyone to introduce themselves and mention
what brings them to ClojureBridge.

## You are here!

This graphic shows you all the tools you will be using today:

![You are here](/assets/images/installfest/YouAreHere-med.png)

## Why Clojure?

* [Why Clojure?](#programming-and-why-clojure)

## Using git to save your programs

* [Using git to save your programs](#git)
