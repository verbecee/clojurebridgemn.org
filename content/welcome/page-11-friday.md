Agenda for Friday
==================================================

## Join us at Prime Digital Academy from 6pm - 8pm

Click [here for a map to Prime Digital Academy](https://www.google.com/maps/dir/''/prime+digital+academy/data=!4m5!4m4!1m0!1m2!1m1!1s0x87f624fd0e5c77af:0x4f615e09b347334e?sa=X&ved=0ahUKEwjNzbLK387YAhVB0YMKHQDwDgEQ9RcIiQEwDg) <br/>
Prime Digital Academy <br/>
301 4th Ave S Suite 577, <br>
Minneapolis, MN 55415

We'll welcome you at the door so you can sign-in, customize your
badge and pickup a lanyard.

## Installfest

The goal of the Installfest is to ensure all the software you
need for the workshop is installed and ready to go. We do this
separately from the workshop because it can sometimes be tricky.
Don't worry -- we'll have plenty of volunteers who are ready to help!

See the detailed instructions on the [Installfest](/installfest) page.

## Dinner

We will provide pizza and soft drinks.

In addition to getting ready for the workshop this is
a great chance to meet the other students and volunteers.

*Don't forget to bring your badge back on Saturday!!!*
