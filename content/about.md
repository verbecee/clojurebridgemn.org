## About ClojureBridge MN
ClojureBridge MN hosts introductory workshops for women, trans, genderqueer, and/or gender non-conforming people interested in learning to program. We teach [Clojure](https://clojure.org/), an expressive, general-purpose programming language. ClojureBridge is emphatically queer and trans friendly. Complete beginners are welcome and encouraged to attend our workshops!

> ## Donate
> Our Minnesota ClojureBridge efforts are part of the [ClojureBridge](http://www.clojurebridge.org) organization which benefits from 501(c)3 non-profit status via affiliation with [Bridge Foundry](https://bridgefoundry.org/). All donations are provided as [unrestricted funds](https://rebrand.ly/bridge-foundry-unrestricted-funds).

> To learn more about corporate sponsorship opportunities please send an e-mail to [info@ClojureBridgeMN.org](mailto:info@ClojureBridgeMN.org).
> Or donate directly with PayPal!
>
> <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
>   <input type="hidden" name="cmd" value="_s-xclick">
>   <input type="hidden" name="hosted_button_id" value="5HC5ZQ8RPS8AA">
>   <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
>   <img alt="" border="0" src=" https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
> </form>

## Committee Members

[comment]: # (BEGIN HTML. Using HTML here since I couldn't find a way to use Markdown to get the structure I needed to style these bios.)

<section class="committee-members">
    <div>
        <img src="/assets/images/cbmn-julie.jpg" alt="Photo of Julie">
        <p>
            Julie has 25+ years of business operations, technical support, customer service, and finance experience. After attending her first ClojureBridge, she decided to pursue her dream to work in software and web development. Her experience is in WordPress and full-stack JavaScript development, and dabbles in Clojure. She has a passion for helping women get involved in tech careers and volunteers at many local tech events. In her free time Julie enjoys spending time with her family and friends, riding motorcycles, practicing yoga, and walking her dog.
        </p>
    </div>
    <hr>
    <div>
        <img src="/assets/images/cbmn-laura.jpg" alt="Photo of Laura">
        <p>
            Laura is a writer, communications specialist, avid techie, and Trekkie. As a word nerd and stickler for details, she finds great satisfaction in auditing websites for typos, poor grammar, weird formatting, broken links - you name it, she’ll find it. Laura enjoys volunteering as a Sighted Guide for a local non-profit and is currently training for her first triathlon.
        </p>
    </div>
    <hr>
    <div>
        <img src="/assets/images/cbmn-elisa.jpg" alt="Photo of Elisa">
        <p>
            Elisa is a wannabe Clojure programmer who loves to learn and share that passion with others. Elisa has a not-so-secret mission of making all of their friends’ children coders. The majority of their day is spent thinking about their cat, Muddy.
        </p>
    </div>
    <hr>
    <div>
        <img src="/assets/images/cbmn-chris.jpg" alt="Photo of Chris">
        <p>
            Chris is an old lisp enthusiast. He likes programming and coding and writing software, despite being a professional developer.
        </p>
    </div>
    <hr>
    <div>
        <img src="/assets/images/cbmn-joe.jpg" alt="Photo of Joe">
        <p>
            Joe is a Software Engineer focused on balancing the best tool for the job, the needs of the business, and the impact on users and maintainers of software.
        </p>
    </div>
</section>

[comment]: # (END HTML)
